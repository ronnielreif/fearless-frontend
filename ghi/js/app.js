function formatDate(dateString) {
    const date = new Date(dateString);
    const year = date.getFullYear();
    const month = (date.getMonth() + 1).toString().padStart(2, '0');
    const day = date.getDate().toString().padStart(2, '0');
    return `${month}/${day}/${year}`;
}

function createCard(name, description, pictureUrl, start, end, location) {
    const formattedStart = formatDate(start);
    const formattedEnd = formatDate(end);

    return `
        <div className="col-md-4">
            <div className="card shadow p-3 mb-5 bg-body-tertiary rounded">
                <img src="${pictureUrl}" className="card-img-top">
                <div className="card-body">
                    <h5 className="card-title">${name}</h5>
                    <p className="card-subtitle text-muted">${location}</p>
                    <p className="card-text">${description}</p>
                </div>
                <div className="card-footer">
                    <small className="text-muted">${formattedStart} - ${formattedEnd}</small>
                </div>
            </div>
        </div>
    `;
}

window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    const errorAlert = document.createElement('div');
    errorAlert.className = 'alert alert-danger d-none';
    errorAlert.role = 'alert';
    document.body.appendChild(errorAlert);


    try {
        const response = await fetch(url);

        if (!response.ok) {
            console.log(response);
        } else {
            const data = await response.json();
            const cardContainer = document.querySelector('.row');

            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);

                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const name = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const start = details.conference.starts;
                    const end = details.conference.ends;
                    const location = details.conference.location.name;
                    const html = createCard(name, description, pictureUrl, start, end, location);
                    cardContainer.innerHTML += html;
                }
            }
        }
    } catch (e) {
        console.log("Error:", e.message);
        errorAlert.textContent = `Error: ${e.message}`;
        errorAlert.classList.remove('d-none');
    }
});

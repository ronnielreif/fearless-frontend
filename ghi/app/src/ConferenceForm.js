import React, { useEffect, useState } from 'react';

function ConferenceForm(props) {

    const [locations, setLocations] = useState([]);
    const [name, setName] = useState('');
    const [start, setStart] = useState('');
    const [end, setEnd] = useState('');
    const [description, setDescription] = useState('');
    const [maxPresentation, setMaxPresentation] = useState('');
    const [maxAttendee, setMaxAttendee] = useState('');
    const [location, setLocation] = useState('');

    const handleNameChange = (event) => {
      const value = event.target.value;
      setName(value);
  }

    const handleStartChange = (event) => {
      const value = event.target.value;
      setStart(value);
  }

    const handleEndChange = (event) => {
      const value = event.target.value;
      setEnd(value);
  }

    const handleDescriptionChange = (event) => {
      const value = event.target.value;
      setDescription(value);
  }

    const handleMaximumPresentationChange = (event) => {
      const value = event.target.value;
      setMaxPresentation(value);
  }

    const handleMaximumAttendeesChange = (event) => {
      const value = event.target.value;
      setMaxAttendee(value);
  }

    const handleLocationChange = (event) => {
      const value = event.target.value;
      setLocation(value);
  }


    const fetchData = async () => {
      const url = 'http://localhost:8000/api/locations/';
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        setLocations(data.locations);
    }
  }

    const handleSubmit = async (event) => {
      event.preventDefault();

      const data = {};
      data.name = name;
      data.starts = start;
      data.ends = end;
      data.description = description;
      data.max_presentations = maxPresentation;
      data.max_attendees = maxAttendee;
      data.location = location;
      console.log(data);

      const locationUrl = 'http://localhost:8000/api/conferences/';
      const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
        },
      };

      const response = await fetch(locationUrl, fetchConfig);
      if (response.ok) {
        const newLocation = await response.json();
        console.log(newLocation);
        setName('');
        setStart('');
        setEnd('');
        setDescription('');
        setMaxPresentation('');
        setMaxAttendee('');
        setLocation('');
      }
    }


    useEffect(() => {
      fetchData();
    }, []);

    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new Conference</h1>
            <form onSubmit={handleSubmit} id="create-location-form">
                <div className="form-floating mb-3">
                    <input
                        onChange={handleNameChange}
                        value={name} placeholder="Name"
                        required type="text"
                        name="name"
                        id="name"
                        className="form-control"/>
                    <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input
                        onChange={handleStartChange}
                        value={start}
                        placeholder="Start"
                        required type="date"
                        name="start"
                        id="start"
                        className="form-control"/>
                    <label htmlFor="start">Starts</label>
                </div>
                <div className="form-floating mb-3">
                    <input
                        onChange={handleEndChange}
                        value={end}
                        placeholder="End"
                        required type="date"
                        name="end" id="end"
                        className="form-control"/>
                    <label htmlFor="end">Ends</label>
                </div>
                <div className="form-group mb-3">
                    <label htmlFor="description">Description</label>
                    <textarea className="form-control"
                      onChange={handleDescriptionChange}
                      value={description}
                      placeholder="Description"
                      id="exampleFormControlTextarea1"
                      rows="3">
                    </textarea>
                </div>
                <div className="form-floating mb-3">
                    <input
                        onChange={handleMaximumPresentationChange}
                        value={maxPresentation}
                        placeholder="Maximum Presentations"
                        required type="number"
                        name="max_presentations"
                        id="maximum_presentations"
                        className="form-control"/>
                    <label htmlFor="maximum_presentations">Maximum presentations</label>
                </div>
                <div className="form-floating mb-3">
                    <input
                        onChange={handleMaximumAttendeesChange}
                        value={maxAttendee}
                        placeholder="Maximum Attendees"
                        required type="number"
                        name="max_attendees"
                        id="maximum_attendees"
                        className="form-control"/>
                    <label htmlFor="maximum_attendees">Maximum attendees</label>
                </div>
                <div className="mb-3">
                <select
                    onChange={handleLocationChange}
                    value={location}
                    required name="location"
                    id="location"
                    className="form-select">
                <option value="">Choose a location</option>
                {locations.map(location => {
                  return (
                    <option
                      key={location.id}
                      value={location.id}>
                      {location.name}
                    </option>
                    );
                })}

                </select>
                </div>
                <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default ConferenceForm
